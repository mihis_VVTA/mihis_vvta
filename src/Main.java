import repository.Repository;
import service.QuizService;
import ui.QuizUI;
import validation.QuizValidator;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws Exception{
       String fileName = "data/quizzes.txt";

        Repository repo = null;
        QuizValidator validator =new QuizValidator();
        try {
            repo = new Repository(fileName, validator);
        } catch (IOException e) {
            e.printStackTrace();
        }

        QuizService ctrl = new QuizService(repo);

       QuizUI console = new QuizUI(ctrl);
       console.run();
    }
}
