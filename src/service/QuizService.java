package service;

import java.util.ArrayList;
import java.util.List;


import domain.Quiz;
import repository.Repository;


public class QuizService {

		
	private Repository repository;

	public QuizService(Repository repository) {
		this.repository = repository;
	}

	public List<Quiz> allQuizzes() {
		return repository.getAll();
	}

    public int maxScore(){
    	int maxScore=0;
    	List<Quiz> quizList = allQuizzes();
		
		for (int i = 0; i < quizList.size(); i++)
			if (quizList.get(i).getCorrectAnswers() > maxScore)
				maxScore = quizList.get(i).getCorrectAnswers();
		
    	return maxScore;
    }

	public List<Quiz> maxScoreQuizList() {
		List<Quiz> quizList = allQuizzes();
		List<Quiz> maxScoreQuizList = new ArrayList<Quiz>();;
		int maxScore = maxScore();
		
		for (int i = 0; i < quizList.size(); i++)
			if (quizList.get(i).getCorrectAnswers() == maxScore)
				maxScoreQuizList.add(quizList.get(i));
		
		return maxScoreQuizList;
	}

	//BBT EP + BVA
	//input: the list of all quizzes
	//output: the number of quizzes that have the maximum no. of correct answers

	/**
	 * determina numarul de teste din lista pentru care s-a obtinut punctajul maxim.
	 * Punctajul unui test reprezinta numarul de intrebari la care s-a raspuns corect si poate fi o valoare de la 0 pana la numarul de intrebari
	 * din test inclusiv. Un test are 30 intrebari. Lista de teste poate sa contina pana la 100 teste.
	 * @param quizList - numarul de puncte obtinut pentru fiecare test
	 * @return numarul de teste pentru care s-a obtinut numarul maxim
	 * @throws QuizException - daca datele de intrare nu sunt valide
	 */

	public int maxScoreQuizCounter() throws QuizException{
		int k, i,p;
		
		List<Quiz>quizList = allQuizzes();

		i=0;k=0;p=0;
	
		while (i<quizList.size()){
			if (quizList.get(i).getCorrectAnswers() > quizList.get(p).getCorrectAnswers()){
				p=i;
				k=1;
			}
			else
				if (quizList.get(p).getCorrectAnswers() == quizList.get(i).getCorrectAnswers())
					k++;
			i++;
		}
		if(quizList.size()==0) return 0;
		return k;
	}	
	
	public void addQuiz(Quiz quiz) {
		
		//repository.save(quiz);
		repository.add(quiz);
		
	}

}
