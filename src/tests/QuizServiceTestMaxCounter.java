package tests;

import domain.Quiz;
import org.junit.*;
import repository.Repository;
import service.QuizException;
import service.QuizService;
import validation.QuizValidator;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class QuizServiceTestMaxCounter {
    static QuizService service;

    @Before
    public void setUp() throws Exception {
        QuizValidator validator = new QuizValidator();
        Repository repo = new Repository(validator);
        service = new QuizService(repo);
    }

    @After
    public void tearDown() throws Exception {
        service = null;

    }
    //EC TC1
    @Test
    public void TC1()  {
        service.addQuiz(new Quiz("id_quiz", 10, "Easy", 1));
        try {
            assertEquals(1, service.maxScoreQuizCounter());
        } catch (QuizException e) {
            e.printStackTrace();
        }
    }

    //@Ignore
    //EC TC2
    @Test
    public void TC2()  {
        //service.addQuiz(new Quiz("id_quiz", 10, "Easy", 1));
        try {
            assertEquals(-1, service.maxScoreQuizCounter());
        } catch (QuizException e) {
            e.printStackTrace();
        }
    }

    @BeforeClass
    public static void setup(){

    }


    @AfterClass
    public static void teardown(){

    }

}