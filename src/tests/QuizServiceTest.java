package tests;

import domain.Difficulty;
import domain.Quiz;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import repository.Repository;
import service.QuizException;
import service.QuizService;
import validation.QuizValidator;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class QuizServiceTest {

    private QuizService service;
    //private List<Quiz> l;

    @Before
    public void setUp(){
        QuizValidator validator= new QuizValidator();
        Repository repo = new Repository(validator);
        service = new QuizService(repo);
        //l =new ArrayList<>();

    }

    //ECP
    @Test
    public void TC1_test_valid_maxScoreQuizCounter() {
        service.addQuiz(new Quiz("q1", 20, "Easy", 15));
        service.addQuiz(new Quiz("q2", 30, "Medium", 29));
        service.addQuiz(new Quiz("q3", 9, "Easy", 4));
        //service.addQuiz(new Quiz("q4", 5, "Hard", 2));
        //service.addQuiz(new Quiz("q5", 20, "Medium", 9));

        try {
            assertEquals(1, service.maxScoreQuizCounter());
        } catch (QuizException e) {
            fail("data not valid");
        }
    }

    //ECP
    @Test(expected=QuizException.class)
    public void TC4_test_invalid_maxScoreQuizCounter()throws QuizException {
        service.addQuiz(new Quiz("q1", 20, "Easy", -1));
        service.addQuiz(new Quiz("q2", 30, "Medium", 5));
        service.addQuiz(new Quiz("q3", 9, "Easy", 7));
        //service.addQuiz(new Quiz("q4", 5, "Hard", 2));
        //service.addQuiz(new Quiz("q5", 20, "Medium", 9));

        service.maxScoreQuizCounter();
        //try {
        //    assertEquals(1, service.maxScoreQuizCounter());
        //} catch (QuizException e) {
        //    fail("data not valid");
        //}
    }

    //BVA
    @Test(expected=QuizException.class)
    public void test_nonvalid_MaxScoreQuizCounter() throws QuizException{


            assertEquals("empty list", 0, service.maxScoreQuizCounter());

    }

    @After
    public void tearDown(){
        service = null;
    }
}