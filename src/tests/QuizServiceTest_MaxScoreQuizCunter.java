package tests;

import domain.Quiz;
import org.junit.*;
import repository.Repository;
import service.QuizService;
import validation.QuizValidator;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class QuizServiceTest_MaxScoreQuizCunter {

    QuizService service;

    @Before
    public void setUp() throws Exception {
        QuizValidator validator = new QuizValidator();
        Repository repository = new Repository(validator);
        service = new QuizService(repository);
    }

    @After
    public void tearDown() throws Exception {
        service = null;

    }

    //EC- TC1
    @Test
    public void TC1() throws Exception {
        service.addQuiz(new Quiz("q1", 15, "Easy", 7));
        service.addQuiz(new Quiz("q2", 25, "Medium", 8));
        service.addQuiz(new Quiz("q3", 10, "Hard", 8));

        assertEquals(2, service.maxScoreQuizCounter());

    }

    @Test
    public void TC2() throws Exception {

        assertEquals("Expected value -1 instead of "+ service.maxScoreQuizCounter(),1, service.maxScoreQuizCounter());

    }

    @BeforeClass
     public static void setup(){

    }

    @AfterClass
    public static void teardown(){

    }

}